(function () {
	$('.input-placeholder').on('click', function (){
		$(this).next().focus();
	});
	$('.form-control').on('focus', function () {
		if (!$(this)[0].value) {
			$(this).prev().hide();
		}
	});
	$('.form-control').on('blur', function () {
		if (!$(this)[0].value) {
			$(this).prev().show();
		}
	});
})();