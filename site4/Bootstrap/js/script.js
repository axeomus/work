(function () {
	$('.navbar-collapse .hidden-xs input').hide();
	$('.navbar-collapse .hidden-xs button').on('click', function (event){
		event.preventDefault();
		var $this = $(this);
		var $input = $('.navbar-collapse input');

		if (!$input[0].value) {
			$input.toggle();
			$input.focus();
		} else {
			$this.parent().submit();
		}
	});
}())