(function () {
	var $input = $('.form-control.input-lg');
	$input.hide();
	$('.navbar-form button').on('click', function (event) {
		event.preventDefault();
		if (!$input[0].value) {
			$input.toggle();
		} else {
			$(this).parent().submit();
		}
	});
})();